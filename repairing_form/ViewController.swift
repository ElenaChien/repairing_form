//
//  ViewController.swift
//  repairing_form
//
//  Created by elena on 2018/6/14.
//  Copyright © 2018年 elena. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    @IBOutlet weak var inStoreButton: UIButton!
    @IBOutlet weak var goOutButton: UIButton!
    @IBOutlet weak var contractMaintenButton: UIButton!
    @IBOutlet weak var underButton: UIButton!
    @IBOutlet weak var outOfButton: UIButton!
    @IBOutlet weak var breakDownTextView: UITextView!
    @IBOutlet weak var solveTextView: UITextView!
    @IBOutlet weak var picture: UIImageView!
    
    //店內button
    @IBAction func inStoreSelect(_ sender: UIButton) {
        
        if inStoreButton.isSelected == true {
            inStoreButton.isSelected = false
            inStoreButton.setBackgroundImage(#imageLiteral(resourceName: "small_default"), for: .normal)
        }else{
            inStoreButton.isSelected = true
            inStoreButton.setBackgroundImage(#imageLiteral(resourceName: "small_hilight"), for: .normal)
            goOutButton.setBackgroundImage(#imageLiteral(resourceName: "small_default"), for: .normal)
        }
    }
    
    //外出button
    @IBAction func goOutSelect(_ sender: UIButton) {
        if goOutButton.isSelected == true {
            goOutButton.isSelected = false
            goOutButton.setBackgroundImage(#imageLiteral(resourceName: "small_default"), for: .normal)
        }else{
            goOutButton.isSelected = true
            goOutButton.setBackgroundImage(#imageLiteral(resourceName: "small_hilight"), for: .normal)
        }
    }
    
    //合約維護button
    @IBAction func contractMaintenSelect(_ sender: UIButton) {
        if contractMaintenButton.isSelected == true {
            contractMaintenButton.isSelected = false
            contractMaintenButton.setBackgroundImage(#imageLiteral(resourceName: "large_default"), for: .normal)
        }else{
            contractMaintenButton.isSelected = true
            contractMaintenButton.setBackgroundImage(#imageLiteral(resourceName: "large_hilight"), for: .normal)
        }
    }
    
    //保內保外button
    @IBAction func underWarrantySelect(_ sender: UIButton) {
        if underButton.isSelected == true {
            underButton.isSelected = false
            underButton.setBackgroundImage(#imageLiteral(resourceName: "small_default"), for: .normal)
            outOfButton.setBackgroundImage(#imageLiteral(resourceName: "small_hilight"), for: .normal)
        }else{
            underButton.isSelected = true
            underButton.setBackgroundImage(#imageLiteral(resourceName: "small_hilight"), for: .normal)
            outOfButton.setBackgroundImage(#imageLiteral(resourceName: "small_default"), for: .normal)
        }
    }
    
//加照片
    @IBAction func addPicture(_ sender: Any) {
        let imagePickerController = UIImagePickerController()
        imagePickerController.delegate = self
        let imagePickerAlertController = UIAlertController(title: "上傳圖片", message: "請選擇要上傳的圖片", preferredStyle: .actionSheet)
        
        let imageFromLibAction = UIAlertAction(title: "照片圖庫", style: .default) { (Void) in
            if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
                imagePickerController.sourceType = .photoLibrary
                self.present(imagePickerController, animated: true, completion: nil)
            }
        }
        
        let imageFromCameraAction = UIAlertAction(title: "相機", style: .default) { (Void) in
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                imagePickerController.sourceType = .camera
                self.present(imagePickerController, animated: true, completion: nil)
            }else{
                self.noCamera()
            }
        }
        
        let cancelAction = UIAlertAction(title: "取消", style: .cancel) { (Void) in
            imagePickerAlertController.dismiss(animated: true, completion: nil)
        }
        
        imagePickerAlertController.addAction(imageFromLibAction)
        imagePickerAlertController.addAction(imageFromCameraAction)
        imagePickerAlertController.addAction(cancelAction)
        
        present(imagePickerAlertController, animated: true, completion: nil)
    }
    
    func noCamera(){
        let alertVC = UIAlertController(title: "No Camera", message: "Sorry, this device has no camera", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style:.default, handler: nil)
        alertVC.addAction(okAction)
        present(alertVC, animated: true, completion: nil)
    }
    
//    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
//
//    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let image =  info[UIImagePickerControllerOriginalImage] as? UIImage {
            picture.image = image
        }
        else{
            //error message
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let borderColor : UIColor = UIColor(red: 0.85, green: 0.85, blue: 0.85, alpha: 1.0)
        
        breakDownTextView.layer.borderWidth = 0.5
        breakDownTextView.layer.borderColor = borderColor.cgColor
        breakDownTextView.layer.cornerRadius = 5.0
        
        solveTextView.layer.borderWidth = 0.5
        solveTextView.layer.borderColor = borderColor.cgColor
        solveTextView.layer.cornerRadius = 5.0
        
        inStoreButton.setBackgroundImage(#imageLiteral(resourceName: "small_default"), for: .normal)
        //        inStoreButton.isSelected = false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
